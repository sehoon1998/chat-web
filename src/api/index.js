import axios from "axios";
import router from "../router";

axios.interceptors.request.use(
    config => {
        let userName = sessionStorage.getItem('auth')
        if (userName) {  // 每个http header都加上token
            config.headers.Authorization = encodeURIComponent(userName);
        }
        return config;
    },
    err => {
        return Promise.reject(err);
    }
);

//响应拦截器
axios.interceptors.response.use(
    response => {
        if (response.status === 200) { // success
            return response;
        } else if (response.status === 401) { // not authorize
            console.log(response.data);
        } else {
            console.log(response.data);
        }
    },
    error => {
        if (error.response) {
            if (error.response.status === 401) {
                router.replace({name: 'Login'});
            }
        }
        return Promise.reject(error);
    }
);

const api = {
    server: process.env.VUE_APP_server,
    login(params) {
        return axios.post(`${api.server}/login`, params)
    },
    listRoom() {
        return axios.post(`${api.server}/rooms`)
    },
    listRoomUser(params) {
        return axios.post(`${api.server}/roomUsers`, params)
    },
    enterRoom(params) {
        return axios.post(`${api.server}/enterRoom`, params)
    },
    createRoom(params) {
        return axios.post(`${api.server}/createRoom`, params)
    }
};

export default api;
